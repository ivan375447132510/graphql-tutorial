const graphql = require('graphql');
const Movies = require("../models/movie");
const Directors = require("../models/director");
const ObjectId = require('mongodb').ObjectId; 

const { GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLSchema, GraphQLID, GraphQLList, GraphQLNonNull, GraphQLBoolean} = graphql;


const MovieType = new GraphQLObjectType({
    name: 'Movie',
    fields: () => ({
        id: { type: GraphQLID},
        name: { type: new GraphQLNonNull(GraphQLString)},
        genre: { type: new GraphQLNonNull(GraphQLString)},
        directorId: { type: GraphQLID},
        director: { 
            type: DirectoryType,
            resolve({directorId}, args){
                return Directors.findById(directorId);
            }
        },
        rate: { type: GraphQLInt },
        watched: { type: new GraphQLNonNull(GraphQLBoolean) },
    })
})

const DirectoryType = new GraphQLObjectType({
    name: 'Directory',
    fields: () => ({
        id: { type: GraphQLID},
        name: { type: new GraphQLNonNull(GraphQLString)},
        age: { type: new GraphQLNonNull(GraphQLString)},
        movies: { 
            type: new GraphQLList(MovieType),
            resolve({_id}, args) {
                const id = new ObjectId(_id);
                return Movies.find({ directorId: id });
            }
        }
    })
})


const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: () => ({
        addDirector: {
            type: DirectoryType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString)},
                age: { type: new GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, {name, age}) {
                const director = new Directors({
                    name, age
                })
                return director.save();
            }
        },
        addMovie: { 
            type: MovieType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString)},
                genre: { type: new GraphQLNonNull(GraphQLString)},
                directorId: { type: GraphQLID},
                rate: { type: GraphQLInt },
                watched: { type: new GraphQLNonNull(GraphQLBoolean) },
            },
            resolve(parent, {name, genre, directorId, rate, watched}) {
                const movie = new Movies({
                    name, genre, directorId, rate, watched
                })
                return movie.save();
            }
        },
        deleteDirectory: { 
            type: DirectoryType,
            args: { id: { type: GraphQLID }},
            resolve(parent, {id}) {
                return Directors.findByIdAndDelete(id);
            }
        },
        deleteMovie: { 
            type: MovieType,
            args: { id: { type: GraphQLID }},
            resolve(parent, {id}) {
                return Movies.findByIdAndDelete(id);
            }
        },
        updateDirector: {
            type: DirectoryType,
            args: {
                id: { type: GraphQLID},
                name: { type: new GraphQLNonNull(GraphQLString)},
                age: { type: new GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, {id, name, age}) {
                return Directors.findByIdAndUpdate(
                    id,
                    {$set: { name, age}},
                    {new: true}
                    );
            }
        },
        updateMovie: {
            type: MovieType,
            args: {
                id: { type: GraphQLID},
                name: { type: new GraphQLNonNull(GraphQLString)},
                genre: { type: new GraphQLNonNull(GraphQLString)},
                directorId: { type: GraphQLID},
                rate: { type: GraphQLInt },
                watched: { type: new GraphQLNonNull(GraphQLBoolean) },
            },
            resolve(parent, {id, name, genre, directorId, rate, watched}) {
                return Movies.findByIdAndUpdate(
                    id,
                    {$set: { 
                        name, genre, directorId, rate, watched
                    }},
                    {new: true}
                    );
            }
        },
    })
})

const Query = new GraphQLObjectType({
    name: 'Query',
    fields: {
        movie: {
            type: MovieType,
            args: { id: { type: GraphQLID }},
            resolve(parent, {id}) {
                return Movies.findById(id)
            }
        },
        director: {
            type: DirectoryType,
            args: { id: { type: GraphQLID }},
            resolve(parent, {id}) {
                return Directors.findById(id)
            }
        },
        movies: {
            type: new GraphQLList(MovieType),
            args: {name: {type: GraphQLString}},
            resolve(parent, {name}) {
                return Movies.find({name: {$regex: name, $options: "i"}});
            }
        },
        directors: {
            type: new GraphQLList(DirectoryType),
            args: {name: {type: GraphQLString}},
            resolve(parent, {name}) {
                return Directors.find({name: {$regex: name, $options: "i"}});
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: Query,
    mutation: Mutation
    });